import pathlib

from aoc_py import loader

_DEFAULT_INPUT: str = loader.load_for(__name__)


def run_a(data: str = _DEFAULT_INPUT) -> int:
    floor = 0

    for c in list(data):
        if c == "(":
            floor += 1
        elif c == ")":
            floor -= 1

    return floor


def run_b(data: str = _DEFAULT_INPUT) -> int:
    floor = 0
    steps = 0

    for c in list(data):
        if c == "(":
            floor += 1
        elif c == ")":
            floor -= 1

        if c in ["(", ")"]:
            steps += 1
            if floor < 0:
                return steps
