from pathlib import Path

_data_path = Path(Path(__file__).parent.parent.joinpath("data")).absolute()


def load_for(name: str) -> str:
    _, _, year, day = name.split(".")
    input_file = _data_path.joinpath(year, day + ".txt")
    return input_file.read_text()
