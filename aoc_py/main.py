import inspect
import pkgutil
import importlib
import argparse
from typing import Dict, Callable

from aoc_py import years

_parser = argparse.ArgumentParser(description="Run the Advent of Code stuff")

_sub_parsers = _parser.add_subparsers(
    title="subcommands", dest="subcommand", required=True,
)

_list_parser = _sub_parsers.add_parser("list", help="lists all available tasks")


def print_list(args: argparse.Namespace):
    years_path = years.__path__
    for year_info in pkgutil.iter_modules(years_path):
        print("Year {year}:".format(year=year_info.name[1:]))
        year_pkg_path = importlib.import_module(
            "." + year_info.name, years.__package__
        ).__path__
        days = pkgutil.iter_modules(year_pkg_path)

        for day_info in days:
            print("  Day {day:02d}".format(day=int(day_info.name[1:])))


_SUBCOMMANDS: Dict[str, Callable[[argparse.Namespace], None]] = {
    "list": print_list,
}


def main():
    args = _parser.parse_args()

    _SUBCOMMANDS[args.subcommand](args)

    for mod_info in pkgutil.iter_modules(years.__path__):
        pkg = importlib.import_module("." + mod_info.name, years.__package__)
        for mod_info2 in pkgutil.iter_modules(pkg.__path__):
            mod = importlib.import_module("." + mod_info2.name, pkg.__package__)
            print("{}:{}:a: {}".format(mod_info.name, mod_info2.name, mod.run_a()))
            print("{}:{}:b: {}".format(mod_info.name, mod_info2.name, mod.run_b()))
