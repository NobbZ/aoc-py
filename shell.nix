let
  pkgsSrc = builtins.fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/f8248ab.tar.gz";
  pkgs = import pkgsSrc {};

  poetry2nixSrc = builtins.fetchTarball "https://github.com/nix-community/poetry2nix/archive/1.9.2.tar.gz";
  poetry2nix = import poetry2nixSrc {};

  python = pkgs.python38;

  inherit (pkgs) mkShell callPackage;
  inherit (pkgs) lefthook poetry;
  inherit (poetry2nix) mkPoetryEnv;

  aoc = mkPoetryEnv {
    projectDir = ./.;

    editablePackageSources = {
      aoc-py = ./aoc_py;
    };

    inherit python;
  };
in aoc.env.overrideAttrs (oa: {
  buildInputs = [ lefthook poetry ];
})
